var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Mathew Holden'
 });
});
router.get('/about', function(req, res, next) {
  res.render('about', { title: 'About Page'
 });
});
router.get('/form', function(req, res, next) {
  res.render('form', { title: 'Form'
 });
 });
 router.post('/form', function(req, res, next) {
  res.render('results', { 
      title: 'Results',
      name: req.body.name,
      email: req.body.email,
      comment: req.body.comment
  });
});


module.exports = router;
