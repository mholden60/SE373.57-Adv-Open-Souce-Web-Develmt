class Controller {

    constructor(model) {
        this.Model = model
    }

    home() {
       return this.Model.getEmployeeList()
    }
    
    create() {
       this.Model.clearDataBindModel()
        return window.Promise.resolve()
    }

}