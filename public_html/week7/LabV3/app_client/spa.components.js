class Components {
   static employeeTable(data){
        if ( !Array.isArray(data) ) return Promise.resolve('')
        return Promise.resolve(`${data.map(row=>                                         
                    `<tr>
                        <td>${row.firstname}</td>
                        <td>${row.lastname}</td>
                        <td>${row.department}</td>
                        <td>${row.createdOnFormated}</td>
                        <td>${row.jobtitle}</td>
                        <td>$${row.formatSalary}</td>
                        <td><button data-id="${row._id}" data-bind-event="click:deleteEmployee" class="button is-danger is-outlined">Delete</button></td>
                        <td><button data-id="${row._id}" data-bind-event="click:goToUpdatePage" class="button is-link is-outlined">Update</button></td>
                    </tr>`
                ).join('')}`)
        }
    }