class Model extends BaseModel {

    constructor() {
        super()
        this.APIS = {
            Employee: `//${window.location.hostname}:3001/api/v1/employee/`
        }


    }
    saveEmployee(evt) {

        const data = {
            firstname: this.dataBindModel.firstname,
            lastname: this.dataBindModel.lastname,
            department: this.dataBindModel.department,
            startdate: this.dataBindModel.startdate,
            jobtitle: this.dataBindModel.jobtitle,
            salary: this.dataBindModel.salary
        }
        return this.http.post(this.APIS.Employee, data)
                .then(data => {
                    this.dataBindModel.saveResultMsg = 'Employee Saved'
                    return data
                }).catch(err => {
            this.dataBindModel.saveResultMsg = 'Employee was NOT Saved'
            return err
        })
    }

    getEmployeeList() {
        return this.http.get(this.APIS.Employee)
                .then(data => {
                    data.forEach((employee) => {
                        employee.createdOnFormated = this.formatDate(employee.startdate)
                        employee.formatSalary = this.formatNumber(employee.salary)
                    })
                    return Components.employeeTable(data).then(html => {
                        return this.dataBindModel.employeeTable = html
                    })
                })
    }
    get isAdded() {
        const msg = this.dataBindModel.saveResultMsg
        return msg && msg.toLowerCase().indexOf('not') === -1 && msg.toLowerCase().indexOf('required') === -1
    }

}