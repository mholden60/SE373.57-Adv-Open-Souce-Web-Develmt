class View {
    
        get home() {
        return Promise.resolve(`<section class="hero is-small spacer is-info">
                    <div class="hero-body">
                        <h1 class="title">Employee List</h1>
                    </div>
                </section>
                <p data-bind-model="deleteResultMsg" data-bind-safe data-bind-class="{'is-success': 'isDeleted', 'is-danger': '!isDeleted' }" class="notification is-spaced"></p>              
                <table class="table is-spaced is-bordered is-hoverable is-fullwidth is-small is-dark">
                  <thead>
                    <tr class="is-selected">
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Department</th>
                        <th>Start Date</th>
                        <th>Job Title</th>
                        <th>Salary</th>
                        <th></th>
                        <th></th>
                    </tr>
                  </thead>
                  <tbody data-bind-model="employeeTable"></tbody>
              </table>`)
    }
    get create(){
        return Promise.resolve(
                `
                <h1>Create Employee</h1>
        
        First Name<input type="Text" name="firstname"/>
        <br/><br/>
        Last Name<input type="Text" name="lastname"/>
        <br/><br/>
        Department<select name="department">
            <option value=" "></option>
            ${['Software Development','Networking','Data Analyst'].map(txt=>`<option value="${txt}">${txt}</option>`).join('')}       
            </select>
        <br/><br/>
         Start Date<input type="date" name="startdate">
        <br/><br/>
        Job Title <input type="text" name="jobtitle" id="jobtitle">
        <br/><br/>
        Salary <input type="number" name="salary" id="salary">
        <br/><br/>
        <input type="button" value="submit" class="button is-link" data-bind-event="click:saveEmployee" />

<p data-bind-model="saveResultMsg" data-bind-safe data-bind-class="{'is-success': 'isAdded', 'is-danger': '!isAdded' }" class="notification"></p> 
        
`)
    }
}