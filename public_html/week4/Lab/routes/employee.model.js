var mongoose = require('mongoose');
var employeeSchema = new mongoose.Schema({
    firstname: {
        type: String,
        required: [true, 'First Name is required']
    },
    lastname: {
        type: String,
        required: [true, 'Last Name is required']
    },
    department: {
        type: String,
        required: [true, 'A Department is required']
    },
    startdate: {
        type: Date,
        required: [true, 'Date is required']
    },
    jobtitle: {
        type: String,
        required: [true, 'Job Title is required']
    },
    salary: {
        type: Number,
        required: [true, 'A Salary is required']
    }
});

var Employee = mongoose.model('Review', employeeSchema);

module.exports = Employee;



