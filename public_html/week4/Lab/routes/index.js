//var express = require('express');
//var router = express.Router();
//
///* GET home page. */
//router.get('/', function(req, res, next) {
//  res.render('index', { title: 'Employee Form' });
//});
//router.get('/view', function(req, res, next) {
//  res.render('view', { title: 'View Employees'
// });
//});
//router.post('/index', function(req, res, next) {
//    
// req.checkBody('firstname', 'First Name is required').notEmpty();
// req.checkBody('lastname', 'Last Name is required').notEmpty();
// req.checkBody('jobtitle', 'Job Title is required').notEmpty();
// req.checkBody('salary', 'Salary is required').notEmpty();
// 
// //Trim and escape the name field. 
//    req.sanitize('email').escape();
//    req.sanitize('email').trim();
//    
//    //Run the validators
//var errors = req.validationErrors();
//    debug(errors);
//  res.render('form', { title: req.body.email, errors: errors });
//});
//module.exports = router;
//
var express = require('express');
var router = express.Router();
var ctrlHome = require('./employee.controller');

router.all('/', ctrlHome.home);
router.all('/index', ctrlHome.home);
router.all('/update/:id?', ctrlHome.update);
router.all('/delete/:id?', ctrlHome.delete);
router.all('/view', ctrlHome.view);


module.exports = router;