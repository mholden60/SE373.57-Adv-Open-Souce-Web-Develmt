var Employee = require('./employee.model');
var debug = require('debug')('lab:employee');

module.exports.home = function (req, res) {
    //debug("Function Test");
    //   debug(req.method);

    if (req.method === 'POST') {
        var msg = '';
        Employee.create({
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            department: req.body.department,
            startdate: req.body.startdate,
            jobtitle: req.body.jobtitle,
            salary: req.body.salary
        })
                .then(function () {
                    msg = 'Employee Saved';
                    debug("Employee Was saved");
                    return;
                })
                .catch(function (err) {
                    msg = 'Employee not saved';
                    debug("Employee Was not saved");
                    return err;
                }).then(function (err) {
            debug(err);
            if (err) {

                res.render('index', {
                    title: 'Employee Error',
                    errMessage: msg,
                    firstnameMessage: err.errors.firstname,
                    lastnameMessage: err.errors.lastname,
                    deptartmentMessage: err.errors.department,
                    startDateMessage: err.errors.startdate,
                    jobTitleMessage: err.errors.jobtitle,
                    salaryMessage: err.errors.salary
                });
            }
            else {
                res.render('index', {
                    title: "home",
                    message: msg
                });
            }
        });

    }
    else {
        res.render('index', {
            title: 'Employee',
            message: ''
        });
    }
}
module.exports.view = function (req, res) {


    Employee
            .find()
            .exec()
            .then(function (results) {
                res.render('view', {
                    title: 'View Results',
                    results: results
                });
            });



}
module.exports.delete = function (req, res) {
    var id = req.params.id,
            removed = 'ID not found';
    if (id) {
        Employee.remove({_id: id})
                .then(function () {
                    removed = '${id} has been removed';
                    finish();
                })
                .catch(function (err) {
                    removed = '${id} has not been removed';
                    finish();
                });
    } else {
        finish();
    }

    function finish() {
        //no id found
        res.render('delete', {
            title: removed
        });
    }
};
module.exports.update = function(req, res){
    
    var id = req.params.id;
    var msg = '';

    if (req.method === 'POST') {
         
        id = req.body._id;

        Employee
            .findById(id)
            .exec()
            .then(function(empData) {
                // DEBUG BODY!
                debug(req.body);
       
                empData.firstname = req.body.firstname;
                empData.lastname = req.body.lastname;
                empData.department = req.body.department;
                empData.startdate = new Date(req.body.startdate + ' EDT'),
                empData.jobtitle = req.body.jobtitle;
                empData.salary = req.body.salary;

                return empData.save();
                                
            })
            .then(function(){
                msg = 'data has been updated';
                finish();
            })
            .catch(function(){
                msg = 'data has NOT been updated';
                finish();
            });
        
    }
    else {
        finish();
    }
    function finish() {
        Employee
                .findOne({'_id': id})
                .exec()
                .then(function (results) {
                    res.render('update', {
                        title: 'Update Results',
                        message: msg,
                        results: results
                    });
                })
                .catch(function () {
                    res.render('notfound', {
                        message: 'ID not found'
                    });
                });
    }
}
