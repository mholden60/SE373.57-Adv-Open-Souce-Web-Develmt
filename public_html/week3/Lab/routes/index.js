var express = require('express');
var router = express.Router();
var debug = require('debug')('Lab:index');
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Grid Display',grid: 0 });
});
router.post('/', function(req, res, next) {
  res.render('index', { 
      title: 'Table Generator',
      grid: req.body.numbers
  });
});

module.exports = router;
