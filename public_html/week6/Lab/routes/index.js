var express = require('express');
var router = express.Router();
var ctrlEmployees = require('./employee.controller');

// reviews
router.get('/employee', ctrlEmployees.employeesReadAll);
router.get('/employee/:employeeid', ctrlEmployees.employeesReadOne);
router.post('/employee', ctrlEmployees.employeesCreate);
router.put('/employee/:employeeid', ctrlEmployees.employeesUpdateOne);
router.delete('/employee/:employeeid', ctrlEmployees.employeesDeleteOne);


module.exports = router;
