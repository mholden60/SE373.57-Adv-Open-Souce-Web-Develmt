var Employee = require('./employee.model');
var debug = require('debug')('lab:employee');

function sendJSONresponse(res, status, content) {
    res.status(status);
    res.json(content);
};

module.exports.employeesReadAll = function(req, res) {
        
    debug('Getting all Employees');
    Employee
     .find()
     .exec()
     .then(function(results){
        sendJSONresponse(res, 200, results);
     })
     .catch(function(err){
        sendJSONresponse(res, 404, err);         
     });
    
};

module.exports.employeesReadOne = function(req, res) {
    
    if (req.params && req.params.employeeid) {
        debug('Getting single Employee with id =', req.params.employeeid );
        
        Employee
        .findById(req.params.employeeid)
        .exec()
        .then(function(results){
            sendJSONresponse(res, 200, results);
        }).catch(function(err){
            sendJSONresponse(res, 404, {
                "message": "employeeid not found"
            });
        });

    } else {
        sendJSONresponse(res, 404, {
            "message": "employeeid not found"
        });
    }
};

/*   POST a new review
 *   /api/v1/reviews 
 */
module.exports.employeesCreate = function(req, res) {
    
    debug('Creating an employee with data ', req.body);
    
    Employee.create({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        department: req.body.department,
        startdate: req.body.startdate,
        jobtitle: req.body.jobtitle,
        salary: req.body.salary

    })
    .then(function(dataSaved){
        debug(dataSaved);
        sendJSONresponse(res, 201, dataSaved);
    })
    .catch(function(err){ 
        debug(err);
        sendJSONresponse(res, 400, err);
    });
     
};

module.exports.employeesUpdateOne = function(req, res) {
    
  if ( !req.params.employeeid ) {
    sendJSONresponse(res, 404, {
        "message": "Not found, employeeid is required"
    });
    return;
  }
  
  Employee
    .findById(req.params.employeeid)
    .exec()
    .then(function(employeeData) {        
        employeeData.firstname = req.body.firstName;
        employeeData.lastname = req.body.lastName;
        employeeData.department = req.body.department;
        employeeData.startdate = req.body.startDate;
        employeeData.jobtitle = req.body.jobTitle;
        employeeData.salary = req.body.salary;
        return employeeData.save();
    })
    .then(function(data){
        sendJSONresponse(res, 200, data);
    })
    .catch(function(err){
        sendJSONresponse(res, 400, err);
    });
        
};

module.exports.employeesDeleteOne = function(req, res) {
  if ( !req.params.employeeid) {
    sendJSONresponse(res, 404, {
        "message": "Not found, employeeid is required"
    });
    return;
  }
  
  Employee
    .findByIdAndRemove(req.params.employeeid)
    .exec()
    .then(function(data){
        debug("Employee id " + req.params.employeeid + " deleted");
        debug(data);
        sendJSONresponse(res, 204, null);
    })
    .catch(function(err){
        sendJSONresponse(res, 404, err);
    });
    
};